#!/bin/bash

for i in `ls $1/*.svg`; do
	n=$(basename $i)
	inkscape -e $2/${n%.svg}.png $i
done
